const express=require("express")
//Mongoose is a package that allows creation of Schemas to model our data structures
//Also has access to a number of methods for manipulation our database
const mongoose = require("mongoose");

const app = express();

const port = 3001;
//MongoDB Atlas Connection
//when we want to use local mongoDB/robo3t
//mongoose.connect("mongodb://localhose:27017/databaseName")
mongoose.connect("mongodb+srv://admin:admin@cluster0.lvuwh.mongodb.net/batch164_to-do?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
}
	)
let db = mongoose.connection;

//connection error message
db.on("error", console.error.bind(console, "connection error"));
//connection successful message
db.once("open", () => console.log("We're connected to the cloud database"))


app.use(express.json());

app.use(express.urlencoded({ extended:true }));



//Mongoose Schemas
//schemas determine the structure of the documents to be written in the database
//act as blueprint to our data

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		//Default values are the predefined value for a field if we don't put any value.
		default: "pending"
	}
})

const Task = mongoose.model("Task", taskSchema);

//Routes/endpoints

//Creating a new task

//Business Logic
/*
1. add a functionality to check if there are duplicates tasks
- if the task already exists in the database, we return error
- if the task doesn't exist in the database, we add it in the database
		1. The task data will be coming from the request's body.
		2. Create a new Task object with field/property
		3. save the new object to our database.

*/

app.post("/tasks", (req, res) => {
	Task.findOne({ name: req.body.name }, (err, result) =>{
		//If a document was found and the document's name matches the information sent via the client
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate task found")
		}else{
			//if no document was found
			//create a new task and save it to the database
			let newTask = new Task({
				name: req.body.name
			});

			newTask.save((saveErr, savedTask) => {
				//if there are errors in saving
				if(saveErr){
					return console.error(saveErr)
				}else{
					return res.status(201).send("New task created")
				}
			})
		}
	})
})

//Get all tasks
//Business Logic
/*

1. Find/retrieve all the documents
2. if an error is encountered, print the error
3. if no errors are found, send a success status back to the client and return an array of documents


*/
app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) =>{
		if(err){
			return console.log(err);
		}else{
			return res.status(200).json({
				dataFromMDB: result
			})
		}
	} )
})

/*
Activity:
1. Create a User schema.
2. Create a User model.
3. Create a POST route that will access the "/signup" route that will create a user.
4. Process a Post request at the "/signup" route using postman to register a user.
5. Create a GET route that will return all users.
6. Process a GET request at the "/users" route using postman.
7. Create a git repository named S30.
8. Initialize a local git repository, add the remote link and push to git with the commit message of s30
	activity.
9. Add the link in Boodle names Express js DAta Persistence via Mongoose ODM.

Business Logic in Creating a user "/signup"
1.Add a functionality to check if there are duplicates tasks
	- if the user already exists, return error or "Already registered"
	- if the user does not exist, we add it on our database
		1. The user data will be coming from the req.body
		2. Create a new User object with a "username" and "password" fields/properties
		3. Then save, and add an error handling

*/
const userSchema = new mongoose.Schema({
	name: String,
	email: String,

})

const User = mongoose.model("User", userSchema);


app.post("/users", (req, res) => {
	User.find({ name: req.body.name, email: req.body.email }, (err, result) =>{
		//If a document was found and the document's name matches the information sent via the client
		if(result != null && result.name == req.body.name && result.email == req.body.email){
			return res.send("Duplicate task found")
		}else{
			//if no document was found
			//create a new task and save it to the database
			let newUser = new User({
				name: req.body.name,
				email: req.body.email
			});

			newUser.save((saveErr, savedTask) => {
				//if there are errors in saving
				if(saveErr){
					return console.error(saveErr)
				}else{
					return res.status(201).send("New user created")
				}
			})
		}
	})
})

app.get("/users", (req, res) => {
	User.find({}, (err, result) =>{
		if(err){
			return console.log(err);
		}else{
			return res.status(200).json({
				dataFromMDB: result
			})
		}
	} )
})






app.listen(port, ()=> console.log(`Server running at port ${port}`));